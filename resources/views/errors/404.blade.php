@extends('layouts.app')

@section('content')

<!-- ### HEADER ### -->
<header>
    <a name="top" href="{{ env('APP_URL') }}">
        <img class="img-center" src="/images/header.jpg" height="245px" alt="" title="">
    </a>
</header>

<!-- ### SCREENSHOTS ### -->

<section class="obsah">

    <header>
        <div class="content">
            <div class="open" onclick="openNav()">&#9776;</div>

            {{ $exception->getMessage() }}
        </div>
    </header>

    <article>

        <div class="screenshots">
            <ul>
                <li>Hra, ktorú ste sa pokúšali nájsť neexistuje. Zrejme ide o preklep.</li>
            </ul>
        </div>

    </article>

        <footer> 06.07.2017 &copy; všetky práva vyhradené </footer>

    </section>



@endsection
