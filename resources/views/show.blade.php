@extends('layouts.app')

@section('content')

<!-- ### HEADER ### -->
<header>
    <a name="top" href="{{ env('APP_URL') }}">
@empty($images[0])
    @foreach($images as $nameOne => $screenshotOne)
        <img src="data:image/jpg; charset=utf-8; base64, {{ $screenshotOne }}" class="img-center" alt="{{ $nameOne }}" title="{{ $nameOne }}" />
    @break
    @endforeach
@endempty
    </a>
</header>

<!-- ### SCREENSHOTS ### -->

<section class="obsah">

    <header>
        <div class="content">
            <div class="open" onclick="openNav()">&#9776;</div>

        {{-- STEAM --}}
        @if(is_numeric($screenshot))
            <a href="https://store.steampowered.com/app/{{ $screenshot }}" target="_blank">
        @empty(config('screenshots.steamGames.' . $screenshot))
                ! UNKNOWN GAME !
        @else
            {{ config('screenshots.steamGames.' . $screenshot) }}
        @endempty</a>


        {{-- ANDROID --}}
        @elseif(!is_numeric($screenshot) && substr($screenshot, -2) === '-A')
            {{ substr($screenshot, 0, -2) }}

        {{-- FRAPS --}}
        @elseif(!is_numeric($screenshot) && substr($screenshot, -2) === '-F')
            {{ substr($screenshot, 0, -2) }}

        {{-- UPLAY --}}
        @elseif(!is_numeric($screenshot) && substr($screenshot, -2) !== '-F')
        @endif
            ({{ $totalGameScreens }} pics)
    </header>

    <article>

        <div class="screenshots">
            <ul id="lightgallery">
            @empty($images[0])
                @foreach($images as $name => $screen)
                    @if($screen !== 'thumbnails')
                        <li data-src="data:image/jpg; charset=utf-8; base64, {{ $screen }}">
                            <img src="data:image/jpg; charset=utf-8; base64, {{ $screen }}"
                                class="PopBoxImgSmall @if(!is_numeric($screenshot) &&
                                substr($screenshot, -2) === '-A')) img-portait-size @else img-size @endif" alt="{{ $name }}" title="{{ $name }}"
                                {{-- onclick="Pop(this, 50, 'PopBoxImgBig');" --}}
                            />
                        </li>
                    @endif
                @endforeach
            {{-- @else
                @for($i = 0; $i < $count_screens; ++$i)
                @foreach($images[$i] as $name => $screen)
                @if($screen !== 'thumbnails')
                <li>
                    <img width="253" height="158" src="data:image/jpg; charset=utf-8; base64, {{ $screen }}"
                            class="PopBoxImgSmall" alt="{{ $name }}" title="{{ $name }}"
                            onclick="Pop(this, 50, 'PopBoxImgBig');"
                    />
                </li>
                @endif
                @endforeach
                @endfor --}}
            @endif
            </ul>
        </div>

    </article>

    <footer> 06.07.2017 &copy; všetky práva vyhradené </footer>

</section>

@endsection
