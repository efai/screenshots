@extends('layouts.app')

@section('content')

<!-- ### HEADER ### -->
<header>
    <a name="top" href="{{ env('APP_URL') }}">
        <img class="img-center" src="/images/header.jpg" height="245px" alt="" title="">
    </a>
</header>

<!-- ### SCREENSHOTS ### -->

<section class="obsah">

    <header>
        <div class="content">
            <div class="open" onclick="openNav()">&#9776;</div>

            100% completed games with all achievements ({{ $totalCompletedGames }} pics)
        </div>
    </header>

    <article>

        <div class="screenshots">
            <ul id="lightgallery">
            @foreach($images as $name => $screenshot)
                @if($screenshot <> 'thumbnails')
                <li data-src="data:image/jpg; charset=utf-8; base64, {{ $screenshot }}">
                    <img src="data:image/jpg; charset=utf-8; base64, {{ $screenshot }}"
                         class="PopBoxImgSmall img-size" alt="{{ $name }}" title="{{ $name }}"
                         onclick="Pop(this, 50, 'PopBoxImgBig');"
                    />
                </li>
                @endif
            @endforeach
            </ul>

            <ul class="list">
                <li>001. 02.04.2017 - 27/27 - (16,0 hod.) - STASIS - Deluxe Edition</li>
                <li>002. 08.04.2017 - 43/43 - (50,0 hod.) - Darksiders Warmastered Edition</li>
                <li>003. 01.06.2017 - 50/50 - (90,0 hod.) - Darksiders II Deathinitive Edition</li>
                <li>004. 24.06.2017 - 12/12 - (13,5 hod.) - Tharsis</li>
                <li>005. 26.06.2017 - 39/39 - (43,0 hod.) - Shadow Complex Remastered</li>
                <li>006. 06.07.2017 - 24/24 - ( 8,6 hod.) - Volume</li>
                <li>007. 05.10.2017 - 60/60 - (34,0 hod.) - Beholder</li>
                <li>008. 11.10.2017 - 30/30 - ( 6,1 hod.) - Dark Angels: Masquerade of Shadows</li>
                <li>009. 12.10.2017 - &nbsp;8/&nbsp;8 - ( 7,8 hod.) - The Esoterica: Hollow Earth</li>
                <li>010. 15.10.2017 - &nbsp;8/&nbsp;8 - ( 3,7 hod.) - Shtriga: Summer Camp</li>
                <li>011. 16.10.2017 - &nbsp;8/&nbsp;8 - ( 3,5 hod.) - The Saint: Abyss of Despair</li>
                <li>012. 28.10.2017 - 10/10 - (10,9 hod.) - House of Snark 6-in-1 Bundle</li>
                <li>013. 07.11.2017 - 20/20 - (10 hod.) - Prehistoric Tales</li>
                <li>014. 14.11.2017 - 18/18 - (11 hod.) - Legends of Atlantis: Exodus</li>
                <li>015. 11.12.2017 - &nbsp;8/&nbsp;8 - ( 6,2 hod.) - Sacra Terra: Kiss of Death Collector's Edition</li>
                <li>016. 01.01.2018 - 27/27 - (16 hod.) - Orwell: Keep an Eye On You</li>
                <li>017. 03.01.2018 - 38/38 - (30 hod.) - Mark of the Ninja: Special Edition</li>
                <li>018. 23.01.2018 - 13/13 - (10,7 hod.) - Papers, Please</li>
                <li>019. 26.01.2018 - 33/33 - (12,3 hod.) - Trine Enchanted Edition</li>
                <li>020. 19.02.2018 - 105/105 - (38 hod.) - Deponia: The Complete Journey</li>
                <li>021. 23.02.2018 - 48/48 - (16.1 hod.) - Game of Thrones - A Telltale Games Series</li>
                <li>022. 27.03.2018 - 67/67 - (30 hod.) - Alan Wake</li>
                <li>023. 14.04.2018 - 52/52 - (35 hod.) - Cook, Serve, Delicious</li>
                <li>024. 23.04.2018 - 12/12 - (9,5 hod.) - Alan Wake's American Nightmare</li>
                <li>025. 01.05.2018 - 35/35 - (19 hod.) - Tales from the Borderlands</li>
                <li>026. 22.05.2018 - 12/12 - (13,3 hod.) - Lara Croft and the Guardian of Light</li>
                <li>027. 26.05.2018 - &nbsp;8/&nbsp;8 - (4,7 hod.) - World Keepers: Last Resort</li>
                <li>028. 02.06.2018 - 19/19 - (6,1 hod.) - Clockwork Tales: Of Glass and Ink</li>
                <li>029. 16.06.2018 - 36/36 - (15,2 hod.) - Deponia Doomsday</li>
                <li>030. 20.06.2018 - &nbsp;7/&nbsp;7 - (1 hod.) - The Artifact</li>
                <li>031. 21.06.2018 - 50/50 - (16 hod.) - Remember Me</li>
                <li>032. 07.07.2018 - 18/18 - (5 hod.) - Tibetan Quest: Beyond the World's End</li>
                <li>033. 13.07.2018 - 38/38 - (8,3 hod.) - Rescue Team 7 Collector's Edition</li>
                <li>034. 19.07.2018 - 60/60 - (40 hod.) - RAGE</li>
                <li>035. 25.07.2018 - 27/27 - (5,5 hod.) - Riskers</li>
                <li>036. 08.08.2018 - 23/23 - (12,3 hod.) - Chainsaw Warrior</li>
                <li>037. 18.08.2018 - 24/24 - (3,8 hod.) - Silent Age</li>
                <li>038. 10.09.2018 - 59/59 - (43 hod.) - Sleeping Dogs: Definitive Edition</li>
                <li>039. 06.11.2018 - 17/17 - (3,3 hod.) - Indygo</li>
                <li>040. 15.11.2018 - 36/36 - (17,3 hod.) - Yesterday Origins</li>
                <li>041. 22.11.2018 - 47/47 - (43 hod.) - Batman: Arkham Asylum GOTY Edition</li>
                <li>042. 04.12.2018 - 30/30 - (26 hod.) - Batman: Arkham Origins Blackgate - Deluxe Edition</li>
                <li>043. 27.12.2018 - 27/27 - (8,8 hod.) - Adam Wolfe</li>
                <li>044. 17.02.2019 - 12/12 - (5,8 hod.) - Event[0]</li>
                <li>045. 10.03.2019 - &nbsp;5/&nbsp;5 - (2,0 hod.) - Gone Fireflies</li>
                <li>046. 17.03.2019 - 23/23 - (3,0 hod.) - Z-End</li>
                <li>047. 23.03.2019 - 24/24 - (5,3 hod.) - Crime Secrets: Crimson Lily</li>
                <li>048. 27.03.2019 - 25/25 - (42 hod.) - Dungeon Rushers</li>
                <li>049. 14.04.2019 - 15/15 - (6,5 hod.) - The Turing Test</li>
                <li>050. 19.04.2019 - 15/15 - (6,5 hod.) - The Uncertain: Episode 1 - The Last Quiet Day</li>
                <li>051. 22.04.2019 - 15/15 - (6,5 hod.) - Orwell: Ignorance is Strength</li>
                <li>052. 10.05.2019 - 56/56 - (25 hod.) - Distrust: a Long Dark Polar Survival</li>
                <li>053. 12.05.2019 - 28/28 - (6,2 hod.) - Trine 3: The Artifacts of Power</li>
                <li>054. 08.06.2019 - 28/28 - (7,2 hod.) - Demon Hunter: Chronicles from Beyond</li>
                <li>055. 08.06.2019 - 32/32 - (26 hod.) - Beat Cop</li>
                <li>056. 22.06.2019 - 15/15 - (4,3 hod.) - Demon Hunter 2: New Chapter</li>
                <li>057. 22.06.2019 - 24/24 - (4,4 hod.) - Demon Hunter 3: Revelation</li>
                <li>058. 07.07.2019 - 22/22 - (6,3 hod.) - Demon Hunter 4: Riddles of Light</li>
                <li>059. 07.08.2019 - 19/19 - (8,4 hod.) - Contract with the Devil</li>
                <li>060. 02.09.2019 - 15/15 - (8,5 hod.) - Nightmares from the Deep: The Cursed Heart</li>
                <li>061. 26.09.2019 - 29/29 - (10,4 hod.) - Nightmares from the Deep 2: The Siren's Call</li>
                <li>062. 24.10.2019 - 32/32 - (8,5 hod.) - Nightmares from the Deep 3: Davy Jones</li>
                <li>063. 20.01.2020 - 26/26 - (27 hod.) - Melissa K. and the Heart of Gold Collector's Edition</li>
                <li>064. 09.02.2020 - 16/16 - (3,4 hod.) - Dracula's Legacy</li>
                <li>065. 28.02.2020 - 19/19 - (8,3 hod.) - Dark Arcana: The Carnival</li>
                <li>066. 28.02.2020 - 13/13 - (3,2 hod.) - Left in the Dark: No One on Board</li>
                <li>067. 06.03.2020 - 33/33 - (18,3 hod.) - Do not Feed the Monkeys</li>
                <li>068. 20.03.2020 - 16/16 - (7,3 hod.) - Agent Walker: Secret Journey</li>
                <li>069. 27.04.2020 - 13/13 - (6,2 hod.) - Faces of Illusion: The Twin Phantoms</li>
                <li>070. 29.04.2020 - 13/13 - (5,2 hod.) - Lost Grimoires: Stolen Kingdom</li>
                <li>071. 09.05.2020 - 12/12 - (5,6 hod.) - Lost Grimoires 2: Shard of Mystery</li>
                <li>072. 17.05.2020 - 13/13 - (6,2 hod.) - Lost Grimoires 3: The Forgotten Well</li>
                <li>073. 01.06.2020 - 25/25 - (6,0 hod.) - Eventide: Slavic Fable</li>
                <li>074. 06.06.2020 - 30/30 - (5,8 hod.) - Eventide 2: The Sorcerer's Mirror</li>
                <li>075. 14.06.2020 - 28/28 - (5,3 hod.) - Eventide 3: Legacy of Legends</li>
                <li>076. 17.11.2020 - &nbsp;8/&nbsp;8 - (9,2 hod.) - Hidden Folks</li>
                <li>077. 28.01.2021 - 40/40 - (14,1 hod.) - Syberia 3</li>
                <li>078. 04.02.2021 - 32/32 - (19,1 hod.) - Dex</li>
                <li>079. 11.02.2021 - 65/65 - (33,3 hod.) - Bioshock Remastered</li>
                <li>080. 11.02.2021 - 53/53 - (25,2 hod.) - Bioshock 2 Remastered</li>
                <li>081. 02.04.2021 - 80/80 - (48,7 hod.) - Bioshock: Infinite</li>
                <li>082. 15.04.2021 - 22/22 - (16 hod.) - Adr1ft</li>
                <li>083. 17.04.2021 - 16/16 - (3 hod.) - The White Door</li>
                <li>084. 22.04.2021 - 15/15 - (13 hod.) - Portal</li>
                <li>085. 02.04.2021 - 62/62 - (32,5 hod.) - Duke Nukem Forever</li>
                <li>086. 21.05.2021 - 22/22 - (6,3 hod.) - Queen's Quest: Tower of Darkness</li>
                <li>087. 31.07.2021 - 78/78 - (212,4 hod.) - The Witcher 3: Wild Hunt - Game of the Year Edition</li>
                <li>088. 07.08.2021 - 52/52 - (27,7 hod.) - Beholder 2</li>
                <li>089. 13.08.2021 - 15/15 - (4,6 hod.) - Queen's Quest 2: Stories of Forgotten Past</li>
                <li>090. 16.08.2021 - 18/18 - (19,7 hod.) - 911 Operator</li>
                <li>091. 03.09.2021 - 54/54 - (24,1 hod.) - Train Station Renovation</li>
                <li>092. 09.09.2021 - 15/15 - (14,7 hod.) - Bus Mechanic Simulator</li>
                <li>093. 31.10.2021 - 83/83 - (77,4 hod.) - Saints Row: The Third</li>
            </ul>
        </div>

    </article>

        <footer> 06.07.2017 &copy; všetky práva vyhradené </footer>

    </section>

@endsection
