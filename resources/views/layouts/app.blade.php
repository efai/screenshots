<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="content-type" content="application/xhtml-xml; charset=UTF-8" />
        <meta http-equiv="content-language" content="sk"/>
        <meta name="robots" content="all" />
        <meta name="description" content="Steamshots, Screenshots"/>
        <meta name="keywords" content="steamshots, screenshots"/>
        <meta name="copyright" content="EfaiTech"/>
        <meta name="author" content="Efai"/>

        <title>{{ env('APP_NAME') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="css/lightgallery.css" />

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        {{-- <script src="{{ asset('js/popBox.js') }}" type="text/javascript"></script> --}}

    <body>

        <div id="overlay"></div>

        @include('layouts.sidebar')

        <a href="#top"><div class="back-to-top">NÁVRAT HORE</div>

        <section>

            @yield('content')

        </section>

        <!-- ### FOOTER ### -->
        <footer>
            Webdizajn a Programovanie:
            <strong>
                <a href="http://efai.hys.cz">
                    Efai
                </a>
            </strong>
        </footer>

        <script src="js/lightgallery.min.js"></script>

        <!-- lightgallery plugins -->
        <script src="js/lg-thumbnail.min.js"></script>
        <script src="js/lg-fullscreen.min.js"></script>

        <script type="text/javascript">
            lightGallery(document.getElementById('lightgallery'));
        </script>
    </body>
</html>
