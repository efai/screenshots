<div id="sidenav" class="sidebar">
    <nav style="padding-left: 20px; padding-bottom: 40px;">
        <ul>
            {{-- STEAM --}}
            <li class="menu">
                STEAM - {{ $count['steam'] }} hier
            </li>

            <select class="dropdown" onchange="window.document.location.href=this.options[this.selectedIndex].value;">
                <option> &lt; Select Steam game &gt; </option>

                @foreach($steamGamesWithoutName as $noNameSteamGame)
                    <option value="{{ $noNameSteamGame }}"> {{ $noNameSteamGame }} </option>
                @endforeach

                @foreach($steamGames as $index_steam => $steamgame)
                    <option value="{{ $index_steam }}"> {{ $steamgame }} </option>
                @endforeach
            </select>

            {{-- UPLAY --}}
            <li class="menu">
                UPLAY - {{ $count['uplay'] }} hier
            </li>

            <select class="dropdown" onchange="window.document.location.href=this.options[this.selectedIndex].value;">
                <option> &lt; Select Uplay game &gt; </option>

                @foreach($uplayGames as $index_uplay => $game_uplay)
                    <option value="{{ $uplayGames[$index_uplay] }}"> {{ $game_uplay }} </option>
                @endforeach
            </select>

            {{-- FRAPS --}}
            <li class="menu">
                FRAPS - {{ $count['fraps'] }} hier
            </li>

            <select class="dropdown" onchange="window.document.location.href=this.options[this.selectedIndex].value;">
                <option> &lt; Select Fraps game &gt; </option>

                @foreach($frapsGames as $index_fraps => $game_fraps)
                    <option value="{{ $frapsGames[$index_fraps] . '-F' }}"> {{ $game_fraps }} </option>
                @endforeach
            </select>

            {{-- ANDROID --}}
            <li class="menu">
                ANDROID - {{ $count['android'] }} hier
            </li>

            <select class="dropdown" onchange="window.document.location.href=this.options[this.selectedIndex].value;">
                <option> &lt; Select Android game &gt; </option>

                @foreach($androidGames as $index_android => $game_android)
                    <option value="{{ $androidGames[$index_android] . '-A' }}"> {{ $game_android }} </option>
                @endforeach
            </select>
        </ul>
    </nav>
</div>

<script>
    let sidenav = document.querySelector('#sidenav');
    let overlay = document.querySelector('#overlay');

    function openNav() {
        sidenav.style.width = '340px';
        overlay.style.display = 'block';

        overlay.addEventListener('click', function () {
            closeNav();
        });
    }

    function closeNav() {
        sidenav.style.width = '0';
        overlay.style.display = 'none';
    }
</script>
