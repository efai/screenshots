require('./bootstrap');

popBoxWaitImage.src = 'images/popBox/spinner40.gif';
popBoxRevertImage = 'images/popBox/magminus.gif';
popBoxPopImage = 'images/popBox/magplus.gif';

myID = document.querySelector('.back-to-top');

let myScrollFunc = function() {
    let y = window.scrollY;
    if (y >= 600) {
        myID.className = 'back-to-top show';
    } else {
        myID.className = 'back-to-top hide';
    }
};

window.addEventListener('scroll', myScrollFunc);
