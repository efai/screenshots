<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ScreenshotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // COMPLETED
        $completed_dir = config('screenshots.dirs.completed');

        // count how many screens are in the folder
        $totalCompletedGames = count(glob($completed_dir . '\*'));

        // scan folder
        $scanCompletedScreenshots = scandir(config('screenshots.dirs.completed'), SCANDIR_SORT_NONE);

        // exclude from scan '.', '..' and manifest
        $scanedCompletedScreenshots = array_diff($scanCompletedScreenshots, array('.', '..', 'manifest.json'));

        // loop through images as raw data, encode it to base64 and save it to the array (this way, you can display images from different harddisk in web)
        foreach($scanedCompletedScreenshots as $screen) {
            $images[$screen] = base64_encode(file_get_contents($completed_dir . $screen));
        }

        return view('index', compact('totalCompletedGames', 'images'));
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed    $screenshot int or string representation of name of the game
     * @return \Illuminate\Http\Response
     */
    public function show($screenshot)
    {
        // Steam (has numeric designation of games)
        if (is_numeric($screenshot)) {
            $dir = config('screenshots.dirs.steam') . $screenshot . '\screenshots';

        // Fraps
        } elseif(!is_numeric($screenshot) && substr($screenshot, -2) === '-F') {
            $dir = config('screenshots.dirs.fraps') . substr($screenshot, 0, -2);

        // Android
        } elseif(!is_numeric($screenshot) && substr($screenshot, -2) === '-A') {
            $dir = config('screenshots.dirs.android') . substr($screenshot, 0, -2);

        // Uplay
        } elseif(!is_numeric($screenshot) && substr($screenshot, -2) !== '-F') {
            $dir = config('screenshots.dirs.uplay') . $screenshot;

        // not existing
        } else {
            abort(404, 'What are you trying to do man, this type of games doesn\'t exists ?!');
        }

        $totalGameScreens = (count(glob($dir . '\*.{jpg,png,gif}', GLOB_BRACE)));

        /* <!--== #/ start -> scan folder contents ==--> */

        if (! is_dir($dir)) {
            return abort(404, 'Požadovaná hra neexistuje');
        }

        // scan folder for screenshots
        $screenshots = scandir($dir, SCANDIR_SORT_NONE);

        // exclude from scan '.', '..' and manifest
        $screens = array_diff($screenshots, ['.', '..', 'manifest.json', 'Thumbs.db']);

        // reset keys of array
        $screens = array_values($screens);

        /* <!--== /# end -> scan folder contents ==--> */

        // Uplay screenshots needs to sort
        if (strpos($dir, 'Uplay') || strpos($dir, 'Fraps')) {
            natcasesort($screens);
        }

        // fill $images with all screenshots
        foreach($screens as $screen) {
            if ($screen !== 'thumbnails') {
                $images[$screen] = base64_encode(file_get_contents($dir . '\\' . $screen));
            }

            ini_set('memory_limit', '-1'); // allow unlimited memory so the memory will never be exhausted
            set_time_limit(0); // execute everything right away without any delay
        }

        return view('show', compact('screenshot', 'totalGameScreens', 'images'));
    }
}
