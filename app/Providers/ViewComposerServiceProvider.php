<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->menu();
    }

    private function menu()
    {
        view()->composer(['layouts.app', 'layouts.app2'], function($view) {
            $platforms = config('screenshots.platforms');
            $dirs = config('screenshots.dirs');

            foreach($platforms as $key => $platform) {
                // count, how many folders have the dir
                $count[$key] = count( glob($dirs[strtolower($platform)] . '*', GLOB_ONLYDIR) );

                /* <!--== #/ start -> scan folder contents ==--> */

                    // scan_steam/scan_uplay/scan_fraps
                    ${'scan_'.$key} = scandir($dirs[strtolower($platform)], SCANDIR_SORT_NONE);

                    // exclude from scan '.', '..' and manifest
                    ${'scaned_'.$key} = array_diff(${'scan_'.$key}, array('.', '..', 'manifest.json'));

                /* <!--== /# end -> scan folder contents ==--> */
            }

            // number => 'game'
            $steamGames = config('screenshots.steamGames');

            // return games that don't have a name set
            $steamGamesWithoutName = array_diff( $scaned_steam, array_flip($steamGames) );

            // reset array keys
            $uplayGames = array_values($scaned_uplay);
            $frapsGames = array_values($scaned_fraps);
            $androidGames = array_values($scaned_android);

            // sort games alphabetically
            natcasesort($steamGames);
            natcasesort($uplayGames);
			natcasesort($frapsGames);
			natcasesort($androidGames);

            // get view
            $view->with(compact(
                'count', // basic
                'steamGames', 'steamGamesWithoutName', // steam
                'uplayGames', // uplay
                'frapsGames', // fraps
                'androidGames' // fraps
            ));
        });
    }
}
